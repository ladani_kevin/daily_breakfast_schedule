import firebase from 'firebase'
import firestore from 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyA1pBSDXY9lLXH38cGtD7iOnr9-1i3-nmg",
  authDomain: "dailyapp-7021b.firebaseapp.com",
  databaseURL: "https://dailyapp-7021b.firebaseio.com",
  projectId: "dailyapp-7021b",
  storageBucket: "dailyapp-7021b.appspot.com",
  messagingSenderId: "486466995012",
  appId: "1:486466995012:web:4a38166915b6063126cd11",
  measurementId: "G-SJNMZ9MJ9C"
};
const firebaseApp = firebase.initializeApp(firebaseConfig);
firebaseApp.firestore().settings({ timestampsInSnapshots: true })
// firebaseApp.firestore().enablePersistence();

// export firestore database
export default firebaseApp.firestore()
